/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/9/20.
//

#ifndef DS_STDGOAL_MODE_TRACKLINE_H
#define DS_STDGOAL_MODE_TRACKLINE_H

#include "action_mode.h"
#include <ds_mx_stdprimitives/TracklineAction.h>
#include <ds_libtrackline/Trackline.h>

namespace ds_stdgoal {

class ModeTrackline : public ActionModeT<ds_mx_stdprimitives::TracklineAction> {
 public:
  typedef ActionModeT<ds_mx_stdprimitives::TracklineAction> ParentT;
  ModeTrackline(const std::shared_ptr<ControlAdapter>& _adapter);
  virtual ~ModeTrackline() = default;

  void setupParameters(ros::NodeHandle& nh) override;
  void onStart(const GoalConstPtr &goal) override;
  void onStop() override;

  // override this function to implement the goal generation
  std::tuple<bool, Feedback, Result, ds_control_msgs::RovControlGoal>
      vehicleStateCallback(const ds_nav_msgs::NavState& vehicleState) override;

 protected:
  using ParentT::adapter_;
  typename ParentT::GoalConstPtr goal_;
  ds_trackline::Projection proj_;
  ds_trackline::Trackline line_;
  double kappa_;
  double sway_;
};

} // namespace ds_stdgoal

#endif //DS_STDGOAL_MODE_TRACKLINE_