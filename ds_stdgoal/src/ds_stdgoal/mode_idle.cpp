/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/9/20.
//

#include <ds_stdgoal/mode_idle.h>

namespace ds_stdgoal {

ModeIdle::ModeIdle(const std::shared_ptr<ControlAdapter> &_adapter)
    : DurationActionMode<ds_mx_stdprimitives::IdleAction>(_adapter, "action_idle") {
  // Do Nothing
}

void ModeIdle::onStart(const typename ParentT::GoalConstPtr &goal) {
  adapter_->setJoystick(ds_control_msgs::JoystickEnum::STDGOAL);
  adapter_->setControllerMode(ds_control_msgs::ControllerEnum::ROV);
  adapter_->setAllocation(ds_control_msgs::RovAllocationEnum::IDLE); // <-- THIS is different

  goal_ = goal;
  finish_at_ = goal->finish_at;

  visual_ = visualization_msgs::MarkerArray();
}

void ModeIdle::fillInRef(ds_control_msgs::RovControlGoal& ref, const ds_nav_msgs::NavState& vehicleState) {
  // do nothing; ref is already set to invalid.
}

} // namespace ds_stdgoal