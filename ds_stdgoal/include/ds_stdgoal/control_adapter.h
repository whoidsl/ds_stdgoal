/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/13/20.
//

#ifndef DS_STDGOAL_CONTROL_ADAPTER_H
#define DS_STDGOAL_CONTROL_ADAPTER_H

#include <ds_nav_msgs/AggregatedState.h>
#include <ds_nav_msgs/NavState.h>
#include <ds_mxcore/ds_mxparam_geopoint.h>
#include <ds_param/ds_param.h>
#include <ds_param/ds_param_conn.h>
#include <ds_control_msgs/ControllerEnum.h>
#include <ds_control_msgs/RovAllocationEnum.h>
#include <ds_control_msgs/JoystickEnum.h>
#include <ds_mx_stdprimitives/Speed.h>
#include <ds_mx_stdprimitives/Depth.h>
#include <ds_libtrackline/Projection.h>
#include <ds_control_msgs/RovControlGoal.h>
#include <ds_control/goal_base.h>
#include <ds_sensor_msgs/Ranges3D.h>
#include <visualization_msgs/MarkerArray.h>

namespace ds_stdgoal {

/// \brief This call provides a single, common interface to one or more ActionServers that implement
/// individual primitives
class ControlAdapter {
 public:
  ControlAdapter();

  void setupParameters(ros::NodeHandle& nh, ros::NodeHandle& nhp);
  void setupPublishers(ros::NodeHandle& nh);
  void setupSubscriptions(ros::NodeHandle& nh);
  const std::string& controlFrameId() const;
  const std::string& mapFrameId() const;

  void setControllerMode(int controller);
  void setAllocation(int allocation);
  void setJoystick(int joystick = ds_control_msgs::JoystickEnum::STDGOAL);
  bool isActiveJoystick(int my_joystick = ds_control_msgs::JoystickEnum::STDGOAL) const;

  // send a zero thrust command; used at the end of a primitive
  void sendZero();

  const ds_trackline::Projection& controllerProjection() const;
  const ds_nav_msgs::NavState& getLastNavState() const;

  static ds_nav_msgs::AggregatedState invalidGoal();
  static void markInvalid(ds_nav_msgs::FlaggedDouble& val);
  static void markValid(ds_nav_msgs::FlaggedDouble& val, double setpoint);

  void initSpeedRef(const ds_nav_msgs::NavState& state, const ds_mx_stdprimitives::Speed& speed);
  void updateSpeedRef(ds_control_msgs::RovControlGoal& ref, const ds_nav_msgs::NavState& state,
      const ds_mx_stdprimitives::Speed& speed);
  void initDepthRef(const ds_nav_msgs::NavState& state, const ds_mx_stdprimitives::Depth& depth);
  void updateDepthRef(ds_control_msgs::RovControlGoal& ref, const ds_nav_msgs::NavState& state,
      const ds_mx_stdprimitives::Depth& depth);

  void setGoalNode(ds_control::GoalBase* ptr);
  void updateNavState(const ds_nav_msgs::NavState& state);

  void publishExtendedGoal(const ds_control_msgs::RovControlGoal& ref);

  double pick_hdg_rate(double state, double state_rate, double goal,
      double goal_rate=std::numeric_limits<double>::quiet_NaN()) const;
  double pick_depth_rate(double state, double state_rate, double goal,
      double goal_rate=std::numeric_limits<double>::quiet_NaN()) const;

  // TODO: add a pointer to all action servers so we can stop everything

  void _ranges_callback(const ds_sensor_msgs::Ranges3D& msg);

  void publishVisual(const visualization_msgs::MarkerArray& visual);
  void clearVisual();

 protected:
  ds_param::ParamConnection::Ptr conn_;
  ds_param::IntParam::Ptr active_controller_;
  ds_param::IntParam::Ptr active_allocation_;
  ds_param::IntParam::Ptr active_joystick_;

  ros::Publisher goal_pose_pub;
  ros::Publisher extended_goal_publisher_;
  ros::Publisher visualization_pub_;
  ros::Subscriber ranges_sub_;
  std::string control_frame_id_;
  std::string map_frame_id_;

  ds_sensor_msgs::Ranges3D last_range;
  double current_altitude;
  ds_nav_msgs::NavState last_state;
  ros::Time current_depth_time;
  double current_depth_goal;
  double current_depth_sign;

  double surfaced_depth;

  double hdg_maxrate;
  double depth_maxrate;

  ds_trackline::Projection nav_projection_;

};

} // namespace ds_stdgoal {

#endif //DS_STDGOAL_CONTROL_ADAPTER_H
