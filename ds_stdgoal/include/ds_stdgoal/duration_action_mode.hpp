/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/20/20.
//

#ifndef DS_STDGOAL_DURATION_ACTION_MODE_H
#define DS_STDGOAL_DURATION_ACTION_MODE_H

#include "action_mode.h"
#include <ds_control_msgs/RovControlGoal.h>

namespace ds_stdgoal {

template <typename T>
class DurationActionMode : public ActionModeT<T> {
 public:
  DurationActionMode(const std::shared_ptr<ControlAdapter>& _adapter, const std::string& _name);
  virtual ~DurationActionMode() = default;

  void onStart(const typename ActionModeT<T>::GoalConstPtr &goal) override;
  void onStop() override;

  /// A standard implementation is provided that can be implemented with the four
  /// functions shown below.
  std::tuple<bool, typename ActionModeT<T>::Feedback,
             typename ActionModeT<T>::Result, ds_control_msgs::RovControlGoal>
  vehicleStateCallback(const ds_nav_msgs::NavState &vehicleState) override;

  // This is the API that classes implementing this interface must actually implement
  virtual bool taskDone(const ds_nav_msgs::NavState& vehicleState) const {
    // by default, just check for failure or success
    return taskFailed(vehicleState) || taskSucceeded(vehicleState);
  }
  virtual bool taskSucceeded(const ds_nav_msgs::NavState& vehicleState) const { return false; }
  virtual bool taskFailed(const ds_nav_msgs::NavState& vehicleState) const { return false; }
  virtual double distanceToGo(const ds_nav_msgs::NavState& vehicleState) const { return std::numeric_limits<double>::quiet_NaN(); }
  virtual void fillInRef(ds_control_msgs::RovControlGoal& ref, const ds_nav_msgs::NavState& vehicleState) = 0;

 protected:
  using ActionModeT<T>::adapter_;
  using ActionModeT<T>::visual_;
  ros::Time finish_at_;
  typename ActionModeT<T>::GoalConstPtr goal_;
};

template <typename T>
DurationActionMode<T>::DurationActionMode(const std::shared_ptr<ControlAdapter>& _adapter,
    const std::string& _name)
: ActionModeT<T>(_adapter, _name) {
  // do nothing
}

template <typename T>
void DurationActionMode<T>::onStart(const typename ActionModeT<T>::GoalConstPtr &goal) {
  adapter_->setJoystick(ds_control_msgs::JoystickEnum::STDGOAL);
  adapter_->setControllerMode(ds_control_msgs::ControllerEnum::ROV);
  adapter_->setAllocation(ds_control_msgs::RovAllocationEnum::ROV);

  goal_ = goal;
  finish_at_ = goal->finish_at;

  visual_ = visualization_msgs::MarkerArray(); // initialize to an empty
}

template <typename T>
void DurationActionMode<T>::onStop() {
  adapter_->sendZero();
  adapter_->clearVisual();
}

// override this function to implement the goal generation
template <typename T>
std::tuple<bool, typename ActionModeT<T>::Feedback, typename ActionModeT<T>::Result, ds_control_msgs::RovControlGoal> DurationActionMode<T>::vehicleStateCallback(const ds_nav_msgs::NavState &vehicleState) {
  bool done = false;
  typename ActionModeT<T>::Feedback feedback;
  typename ActionModeT<T>::Result result;
  ds_control_msgs::RovControlGoal ref;
  ref.goal = adapter_->invalidGoal();
  ref.goal.header = vehicleState.header;
  ref.goal.ds_header = vehicleState.ds_header;
  fillInRef(ref, vehicleState);

  // check our end criteria: if finish_at is invalid,
  // or if its before then we're done
  if ( !finish_at_.isValid() || finish_at_.isZero()
      || (finish_at_ < vehicleState.header.stamp) || taskDone(vehicleState)) {
    // INVALID finish time!  Return FAILED
    done = true;
    feedback.running = false;
    feedback.distance_to_go = 0;
    feedback.time_to_go = 0;

    // This "idle" is a success if we finish because of a timeout
    if (taskFailed(vehicleState)) {
      result.success = false;
    } else {
      result.success = (finish_at_ < vehicleState.header.stamp);
    }
  } else {

    // still running!
    done = false;
    feedback.running = true;
    feedback.distance_to_go = distanceToGo(vehicleState);
    feedback.time_to_go = (finish_at_ - vehicleState.header.stamp).toSec();

    result.success = false;
  }

  return std::make_tuple(done, feedback, result, ref);
}

} // namespace ds_stdgoal

#endif //DS_STDGOAL_DURATION_ACTION_MODE_H
