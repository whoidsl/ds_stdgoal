/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/9/20.
//

#ifndef DS_STDGOAL_ACTION_MODE_H
#define DS_STDGOAL_ACTION_MODE_H

#include <actionlib/server/simple_action_server.h>
#include <actionlib/action_definition.h>
#include <visualization_msgs/MarkerArray.h>
#include <ds_nav_msgs/NavState.h>
#include <ds_nav_msgs/AggregatedState.h>
#include "control_adapter.h"

namespace ds_stdgoal {

class ActionMode {
 public:
  ActionMode(const std::string& _name) : action_server_name(_name) {}
  virtual ~ActionMode() = default;

  virtual bool isRunning() const = 0;
  virtual void vehicle_state_cb(const ds_nav_msgs::NavState& vehicleState) = 0;

  virtual void setupParameters(ros::NodeHandle& nh) { /* do nothing by default */ }
  virtual void setupPublishers(ros::NodeHandle& nh) { /* do nothing by default */ }
  virtual void setupSubscriptions(ros::NodeHandle& nh) { /* do nothing by default */ }
  virtual void setupActionServer(ros::NodeHandle& nh) = 0;
  virtual void cancelAll() = 0;
  visualization_msgs::MarkerArray getVisual() const { return visual_; }

  const std::string& actionServerName() const {
    return action_server_name;
  }

 protected:
  std::string action_server_name;
  visualization_msgs::MarkerArray visual_;
};

template<typename ActionSpec>
class ActionModeT : public ActionMode {
  // actionlib provides this handy macro to define easy access to all the different subtypes for the ActionSpec
  // template parameter
 protected:
  ACTION_DEFINITION(ActionSpec)

 public:
  ActionModeT(const std::shared_ptr<ControlAdapter>& _adapter,
      const std::string& _name) : ActionMode(_name), adapter_(_adapter) {}
  virtual ~ActionModeT() = default;

  virtual void onStart(const GoalConstPtr& goal) = 0;
  virtual void onStop() = 0;

  // override this function to implement the goal generation
  virtual std::tuple<bool, Feedback, Result, ds_control_msgs::RovControlGoal>
  vehicleStateCallback(const ds_nav_msgs::NavState& vehicleState) = 0;

  void setupActionServer(ros::NodeHandle& nh) override {
    // default should be fine for every mode
    std::string action_server_path = ros::this_node::getName() + "/" + action_server_name;
    /*
    action_server.reset(new actionlib::SimpleActionServer<ActionSpec>(
        nh, action_server_path, boost::bind(&ActionModeT<ActionSpec>::action_server_callback, this, _1),
        false));
        */
    action_server_.reset(new actionlib::SimpleActionServer<ActionSpec>(nh, action_server_path, false));
    action_server_->registerGoalCallback(boost::bind(&ActionModeT<ActionSpec>::action_server_callback, this));
    ROS_INFO_STREAM("Starting Action Server \"" <<action_server_path <<"\"");
    action_server_->start();
  }

  void cancelAll() override {
    if (!action_server_->isActive()) {
      return;
    }
    ROS_INFO_STREAM("Aborting all actions for server " <<action_server_name);
    action_server_->setAborted();
  }

  // internal; do not override
  void vehicle_state_cb(const ds_nav_msgs::NavState& vehicleState) override {
    if (!action_server_->isActive()) {
      return;
    }

    if (action_server_->isPreemptRequested()) {
      onStop();
      action_server_->setPreempted();
    }

    bool done;
    Feedback feedback;
    Result result;
    ds_control_msgs::RovControlGoal con_goal;

    std::tie(done, feedback, result, con_goal)  = vehicleStateCallback(vehicleState);
    adapter_->publishExtendedGoal(con_goal); // some goals send one last "stop here" command
    if (done) {
      adapter_->clearVisual();
      action_server_->setSucceeded(result);
    } else {
      adapter_->publishVisual(getVisual());
      action_server_->publishFeedback(feedback);
    }
    adapter_->updateNavState(vehicleState);
  }

  void action_server_callback() {
    ROS_INFO_STREAM("ActionServer starting new goal!");
    // TODO: stop all running actions on OTHER action servers
    boost::shared_ptr<const Goal> new_goal(action_server_->acceptNewGoal());

    onStart(new_goal);
  }

  bool isRunning() const override {
    return action_server_->isActive();
  }

  void stop() {
    if (action_server_->isActive()) {
      action_server_->setAborted();
    }
  }

 protected:
  std::unique_ptr<actionlib::SimpleActionServer<ActionSpec>> action_server_;
  std::shared_ptr<ControlAdapter> adapter_;
};

} // namespace ds_stdgoal

#endif //DS_STDGOAL_ACTION_MODE_H
