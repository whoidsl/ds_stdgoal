/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/8/20.
//

#ifndef DS_STDGOAL_STDGOAL_H
#define DS_STDGOAL_STDGOAL_H

#include <ds_control/goal_base.h>
#include <ds_control_msgs/GoalLegState.h>
#include <ds_control_msgs/GoalLegLatLon.h>
#include <boost/optional.hpp>

#include <ds_nav_msgs/AggregatedState.h>
#include <ds_nav_msgs/NavState.h>

namespace ds_stdgoal {

struct StdGoalPrivate; // forward declaration

class StdGoal : public ds_control::GoalBase {
  DS_DECLARE_PRIVATE(StdGoal);
 public:
  StdGoal();
  StdGoal(int argc, char** argv, const std::string& name);
  ~StdGoal() override;

  DS_DISABLE_COPY(StdGoal);

  virtual uint64_t type() const noexcept;

  /// \brief Callback for when vehicle state messages are received
  void vehicleStateCallback(const ds_nav_msgs::NavState& vehicleState);

 protected:
  // setup functions go here
  void setupParameters() override;
  void setupSubscriptions() override;
  void setupPublishers() override;
  void setup() override;

 private:
  std::unique_ptr<StdGoalPrivate> d_ptr_;

};

} // namespace ds_stdgoal

#endif //DS_STDGOAL_STDGOAL_H
