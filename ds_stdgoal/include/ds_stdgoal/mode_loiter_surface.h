/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/9/20.
//

#ifndef DS_STDGOAL_MODE_LOITER_SURFACE_H
#define DS_STDGOAL_MODE_LOITER_SURFACE_H

#include "duration_action_mode.hpp"
#include <ds_mx_stdprimitives/LoiterSurfaceAction.h>

namespace ds_stdgoal {

class ModeLoiterSurface : public DurationActionMode<ds_mx_stdprimitives::LoiterSurfaceAction> {
 public:
  typedef DurationActionMode<ds_mx_stdprimitives::LoiterSurfaceAction> ParentT;
  ModeLoiterSurface(const std::shared_ptr<ControlAdapter>& _adapter);
  virtual ~ModeLoiterSurface() = default;

  void onStart(const typename ParentT::GoalConstPtr &goal) override;
  double distanceToGo(const ds_nav_msgs::NavState& vehicleState) const override;
  bool taskFailed(const ds_nav_msgs::NavState& vehicleState) const override;
  void fillInRef(ds_control_msgs::RovControlGoal& ref, const ds_nav_msgs::NavState& vehicleState) override;
  double fillInXYHdgRef(ds_control_msgs::RovControlGoal& ref, const ds_nav_msgs::NavState& vehicleState);

 protected:
  using ParentT::adapter_;
  using ParentT::goal_;
  ds_trackline::Projection proj_;
  double heading_goal_value_;
  int hits_outside;
  int hits_inside;
};

} // namespace ds_stdgoal

#endif // DS_STDGOAL_MODE_LOITER_SURFACE_H
