/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/9/20.
//

#include <ds_stdgoal/mode_loiter.h>
#include <ds_util/angle.h>

namespace ds_stdgoal {

ModeLoiter::ModeLoiter(const std::shared_ptr<ControlAdapter>& _adapter) : ParentT(_adapter, "action_loiter"),
proj_(0,0) {
  // do nothing
}

void ModeLoiter::onStart(const typename ParentT::GoalConstPtr &goal) {
  ParentT::onStart(goal);

  adapter_->initSpeedRef(adapter_->getLastNavState(), goal_->speed_control);
  adapter_->initDepthRef(adapter_->getLastNavState(), goal_->depth_control);

  proj_ = adapter_->controllerProjection();
  heading_goal_value_ = goal_->heading;
  if (goal_->any_heading) {
    heading_goal_value_ = std::numeric_limits<double>::quiet_NaN();
  }
  hits_outside = 0;
  hits_inside = 0;
}

double ModeLoiter::distanceToGo(const ds_nav_msgs::NavState& vehicleState) const {
  ds_trackline::Projection::VectorLL goal_ll;
  goal_ll <<goal_->longitude, goal_->latitude;
  ds_trackline::Projection::VectorEN goal_en = proj_.lonlat2projected(goal_ll);

  ds_trackline::Projection::VectorLL state_ll;
  state_ll <<vehicleState.lon, vehicleState.lat;
  ds_trackline::Projection::VectorEN state_en = proj_.lonlat2projected(state_ll);

  auto displacement = state_en - goal_en;
  return displacement.norm();
}

bool ModeLoiter::taskFailed(const ds_nav_msgs::NavState& vehicleState) const {
  return false;
  // TODO
  return hits_outside >= 100;
}

double ModeLoiter::fillInXYHdgRef(ds_control_msgs::RovControlGoal& ref, const ds_nav_msgs::NavState& vehicleState) {
  // do some projections to put everything in the controller frame
  ds_trackline::Projection::VectorLL goal_ll;
  goal_ll << goal_->longitude, goal_->latitude;
  ds_trackline::Projection::VectorEN goal_en = proj_.lonlat2projected(goal_ll);

  ds_trackline::Projection::VectorLL state_ll;
  state_ll << vehicleState.lon, vehicleState.lat;
  ds_trackline::Projection::VectorEN state_en = proj_.lonlat2projected(state_ll);

  auto displacement = state_en - goal_en;
  double to_go = displacement.norm();

  // set position goal
  adapter_->markValid(ref.goal.easting, goal_en(0));
  adapter_->markValid(ref.goal.northing, goal_en(1));


  // set the heading goal
  // The heading goal is a weighted average of pointing towards the point
  // and the requested heading.  The weight is determined by putting the
  // range to goal through a logistic function to make the transition gradual
  double hdg_to_point = atan2(displacement(0), displacement(1));
  // atan2(east, north) gives us heading from GOAL to STATE.  We want reciprocal
  hdg_to_point += M_PI;

  // respect the optional ability to set a heading goal based on our first entry into the circle
  if ((goal_->any_heading || std::isnan(heading_goal_value_))
      && to_go > goal_->radius + goal_->radius_width) {
    heading_goal_value_ = hdg_to_point;
  }

  // compute the weights using the logistic function
  double width = goal_->radius_width / (std::log(0.95 / 0.05));
  double point_weight = 1.0 / (1.0 + std::exp(-(to_go - goal_->radius) / width));

  // now average the two heading goals
  double sin_avg = point_weight * sin(hdg_to_point) + (1.0 - point_weight) * sin(heading_goal_value_);
  double cos_avg = point_weight * cos(hdg_to_point) + (1.0 - point_weight) * cos(heading_goal_value_);
  double heading_goal = ds_util::normalize_heading_radians(std::atan2(sin_avg, cos_avg));

  // actually set the goal
  adapter_->markValid(ref.goal.heading, heading_goal);
  adapter_->markValid(ref.goal.r, adapter_->pick_hdg_rate(
      vehicleState.heading, vehicleState.r, heading_goal));

  // update the visualization
  visualization_msgs::Marker marker;
  marker.type = visualization_msgs::Marker::CYLINDER;
  marker.header.frame_id = adapter_->mapFrameId();
  marker.header.stamp = vehicleState.header.stamp;
  marker.ns = "stdgoal";
  marker.id = 0;
  marker.frame_locked = true;
  marker.pose.orientation.x = 0;
  marker.pose.orientation.y = 0;
  marker.pose.orientation.z = 0;
  marker.pose.orientation.w = 1.0;
  marker.pose.position.x = goal_en(0);
  marker.pose.position.y = goal_en(1);
  marker.pose.position.z = -ref.goal.down.value;
  marker.scale.x = goal_->radius - goal_->radius_width/2.0;
  marker.scale.y = marker.scale.x;
  marker.scale.z = 0.25;
  // update based on depth mode
  switch (goal_->depth_control.depth_mode) {
    case ds_mx_stdprimitives::Depth::DEPTHMODE_DEPTH:
      marker.pose.position.z = -goal_->depth_control.depth;
      break;
    case ds_mx_stdprimitives::Depth::DEPTHMODE_ALTITUDE:
      marker.pose.position.z = -ref.goal.down.value;
      break;
    case ds_mx_stdprimitives::Depth::DEPTHMODE_SURFACE:
      marker.pose.position.z = 0;
      break;

    case ds_mx_stdprimitives::Depth::DEPTHMODE_TRIANGLE:
      double half_range = 0.5*(goal_->depth_control.depth_floor - goal_->depth_control.depth_ceiling);
      double midpt = 0.5*(goal_->depth_control.depth_floor + goal_->depth_control.depth_ceiling);
      marker.pose.position.z = -midpt;
      marker.scale.z = half_range;
      break;
  }
  marker.color.r = 0;
  marker.color.g = 1;
  marker.color.b = 1;
  marker.color.a = 0.7;

  visual_ = visualization_msgs::MarkerArray();
  visual_.markers.push_back(marker);

  marker.id = 1;
  marker.scale.x = goal_->radius + goal_->radius_width/2.0;
  marker.scale.y = marker.scale.x;
  marker.color.r = 0;
  marker.color.g = 1;
  marker.color.b = 1;
  marker.color.a = 0.4;
  visual_.markers.push_back(marker);


  return to_go;
}

void ModeLoiter::fillInRef(ds_control_msgs::RovControlGoal& ref, const ds_nav_msgs::NavState& vehicleState) {

  double to_go = fillInXYHdgRef(ref, vehicleState);

  // check the radius
  if (to_go < goal_->radius) {
    // we're inside the goal radius; go to the desired heading
    hits_inside = std::min(hits_inside+1, 10);
  } else {
    // check fail criteria, but ONLY if we've already reached the goal position
    if (hits_inside >= 10) {
      hits_outside = std::min(hits_outside+1, 100);
      // this will get caught when we check for fail
    }
  }

  adapter_->updateDepthRef(ref, vehicleState, goal_->depth_control);
  adapter_->updateSpeedRef(ref, vehicleState, goal_->speed_control);
  ref.auto_heading = true;
}

} // namespace ds_stdgoal