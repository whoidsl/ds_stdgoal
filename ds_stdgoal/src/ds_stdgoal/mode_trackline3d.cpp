/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 11/18/20.
//

#include <ds_stdgoal/mode_trackline3d.h>
#include <ds_util/ds_util.h>

namespace ds_stdgoal {

ModeTrackline3d::ModeTrackline3d(const std::shared_ptr<ControlAdapter>& _adapter)
    : ParentT(_adapter, "action_trackline3d"), proj_(0,0) {
  kappa_ = 0.3;
  radius_ = 1.5;
  radius_width_ = 1.0;
}

void ModeTrackline3d::setupParameters(ros::NodeHandle& nh) {
  kappa_ = ros::param::param("~trackline_kappa", kappa_);
  radius_ = ros::param::param("~trackline_radius", radius_);
  radius_width_ = ros::param::param("~trackline_radius_width", radius_width_);
}

void ModeTrackline3d::onStart(const GoalConstPtr &goal) {
  ROS_INFO_STREAM("Starting Trackline stdprim!");

  // setup control modes
  adapter_->setJoystick(ds_control_msgs::JoystickEnum::STDGOAL);
  adapter_->setControllerMode(ds_control_msgs::ControllerEnum::ROV);
  adapter_->setAllocation(ds_control_msgs::RovAllocationEnum::ROV);

  // save goal
  goal_ = goal;
  heading_goal_ = std::numeric_limits<double>::quiet_NaN();

  proj_ = adapter_->controllerProjection();
}

void ModeTrackline3d::onStop() {
  adapter_->sendZero();
}

std::tuple<bool, ModeTrackline3d::Feedback, ModeTrackline3d::Result, ds_control_msgs::RovControlGoal>
    ModeTrackline3d::vehicleStateCallback(const ds_nav_msgs::NavState &vehicleState) {
  bool done = false;
  typename ParentT::Feedback feedback;
  typename ParentT::Result result;

  // prepare our return value
  ds_control_msgs::RovControlGoal ref;
  ref.goal = adapter_->invalidGoal();
  ref.goal.header = vehicleState.header;
  ref.goal.ds_header = vehicleState.ds_header;

  // This is a 3D implementation of Dana's favorite line-of-sight controller.  Because it's
  // 3D, there are some annoying singularities when the trackline is directly vertical.  Among
  // other issues, this means we can't use the standard trackline library or the concept of the
  // trackframe.  Everything has to be done in global X/Y as the y/z axes of the trackframe are
  // not well-defined in 3D.  We'll start by computing the vector of the trackline and the vector
  // from the vehicle's current position back to the trackline that's perpendicular to the trackline.
  auto en_start = proj_.lonlat2projected(
      ds_trackline::Projection::VectorLL(goal_->longitude_start, goal_->latitude_start));
  auto en_end = proj_.lonlat2projected(
      ds_trackline::Projection::VectorLL(goal_->longitude_end, goal_->latitude_end));
  auto en_vehicle = proj_.lonlat2projected(
      ds_trackline::Projection::VectorLL(vehicleState.lon, vehicleState.lat));
  // all vectors in North East Down
  Eigen::Vector3d start_pt(en_start(1), en_start(0), goal_->depth_start);
  Eigen::Vector3d end_pt(en_end(1), en_end(0), goal_->depth_end);
  Eigen::Vector3d vehicle_pt(en_vehicle(1), en_vehicle(0), vehicleState.down);

  Eigen::Vector3d line = end_pt - start_pt;
  double line_len = line.norm();
  if (line_len < 0.001) {
    ROS_INFO_STREAM("Trackline < 1mm.  Skipping...");
    ref.goal.heading.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
    ref.goal.heading.value = 0;
    ref.goal.r.valid = true; // don't turn
    ref.goal.r.value = 0;

    feedback.distance_to_go = 0;
    feedback.time_to_go = 0;
    feedback.running = false;

    result.success = false;

    return std::make_tuple(true, feedback, result, ref);
  }

  Eigen::Vector3d line_unit = line / line_len;
  double along_track_dist = line_unit.dot(vehicle_pt - start_pt);


  // check for Done
  if (along_track_dist >= line_len) {
    // we're done!
    done = true;

    ROS_INFO_STREAM("DONE with trackline! " <<along_track_dist <<" > " <<line_len);
    adapter_->markValid(ref.goal.easting, end_pt(1));
    adapter_->markValid(ref.goal.northing, end_pt(0));
    adapter_->markValid(ref.goal.down, end_pt(2));
    adapter_->markValid(ref.goal.surge_u, 0.1);
    adapter_->markValid(ref.goal.sway_v, 0.1);
    adapter_->markValid(ref.goal.heave_w, 0.1);
    if (std::isnan(heading_goal_)) {
      adapter_->markValid(ref.goal.heading, vehicleState.heading);
    } else {
      adapter_->markValid(ref.goal.heading, heading_goal_);
    }
    ref.auto_xy = true;
    ref.auto_depth = true;
    ref.auto_heading = true;

    feedback.distance_to_go = 0;
    feedback.time_to_go = 0;
    feedback.running = false;

    result.success = true;

    return std::make_tuple(done, feedback, result, ref);
  } else {
    done = false;
  }

  Eigen::Vector3d to_line = start_pt + along_track_dist * line_unit - vehicle_pt;
  double oline = to_line.norm();

  // angle between trackline & direction of travel-- limited to 60 degrees
  double converge = std::min(std::fabs(kappa_ * oline), M_PI/3);

  // based on the convergence angel, recover our vector to travel
  Eigen::Vector3d direction = line_unit + tan(converge) * to_line;
  direction.normalize();

  // next, we need the distance along dhat to the goal point.
  double to_intercept = 0;
  if (converge > 0.1) {
    to_intercept = oline / sin(converge);
  } else {
    // now we have to figure out what to do with the case where the convergence
    // angle is basically zero.  We'll re-use the same trick from the 2D
    // trackline to take the limit-- it's a pretty simple l'Hopital's rule
    // derivation
    to_intercept = 1./kappa_;
  }

  Eigen::Vector3d goal_pt = vehicle_pt + to_intercept * direction;
  // vector is in NED.  Stuff directly in goals.
  adapter_->markValid(ref.goal.northing, goal_pt(0));
  adapter_->markValid(ref.goal.easting, goal_pt(1));
  adapter_->markValid(ref.goal.down, goal_pt(2));

  // Next, we have to set our speed goal.  This will be a version of the 2d one,
  // again.
  Eigen::Matrix3d Rbody2world;
  double vehicleHeading = vehicleState.heading*M_PI/180.0;
  Rbody2world <<cos(vehicleHeading), -sin(vehicleHeading), 0,
                sin(vehicleHeading), cos(vehicleHeading), 0,
                         0,                   0,          1;

  Eigen::Vector3d world_velocity_goal = goal_->speed_control.value * direction;
  Eigen::Vector3d body_velocity_goal = Rbody2world.transpose() * world_velocity_goal;

  // we'll just set a velocity vector in the body frame directly.
  adapter_->markValid(ref.goal.surge_u, body_velocity_goal(0));
  adapter_->markValid(ref.goal.sway_v, body_velocity_goal(1));
  adapter_->markValid(ref.goal.heave_w, body_velocity_goal(2));

  // Finally, we have to set our heading goal.  This one's tricky,
  // as we need it to be perfectly valid even for vertical goals.  They key insight is that
  // we need to pick a value that won't flop around even for vehicle positions close to a vertical.
  double course = atan2(direction(1), direction(0));
  if (course < 0) {
    course += 2*M_PI;
  }

  // distance to the goal projected into the 2d horizontal plane
  double togo_2d = to_intercept * sqrt(direction(0)*direction(0) + direction(1)*direction(1));

  // select our radius & width
  double radius = radius_;
  double width = radius_width_;
  double radius_scaling = std::fabs(direction(2));
  if (goal_->heading_radius != 0) {
    radius = goal_->heading_radius;
  }
  if (goal_->heading_radius_width) {
    width = goal_->heading_radius_width;
  }
  if (!goal_->heading_radius_horizontal) {
    // if we're not defined horizontally
    radius *= radius_scaling;
    width *= radius_scaling;
  }

  // select the alternate heading goal that gets mixed in
  if (togo_2d > radius_ + radius_width_ || std::isnan(heading_goal_)) {
    if (std::isnan(goal_->heading)) {
      heading_goal_ = vehicleHeading;
    } else {
      heading_goal_ = goal_->heading*M_PI/180.0;
    }
  }

  // we'll re-use the logistic function trick here, with a variable radius
  // we also scale the radius_width_ and radius_ from their nominal value by the z-component of the
  // direction vector.  This effectively "turns down/off" the radius when the vehicle's motion is predominantly
  // horizontal, but widens it up as the vehicle moves more vertically.
  double width_param = width / (std::log(0.95 / 0.05));
  double point_weight = 1.0 / (1.0 + std::exp(-(togo_2d - radius) / width_param));

  // now average the two heading goals
  double sin_avg = point_weight * sin(course) + (1.0 - point_weight) * sin(heading_goal_);
  double cos_avg = point_weight * cos(course) + (1.0 - point_weight) * cos(heading_goal_);
  double heading_goal = ds_util::normalize_heading_radians(std::atan2(sin_avg, cos_avg));

  // Force the heading to the specified value, if so commanded
  if (!std::isnan(goal_->heading) && goal_->force_heading) {
    heading_goal = goal_->heading*M_PI/180.0;
  }

  // just use all the autos
  ref.auto_xy = true;
  ref.auto_depth = true;
  ref.auto_heading = true;

  // actually set the goal
  adapter_->markValid(ref.goal.heading, heading_goal);
  adapter_->markValid(ref.goal.r, adapter_->pick_hdg_rate(
      vehicleState.heading, vehicleState.r, heading_goal));

  feedback.distance_to_go = line_len - along_track_dist;
  feedback.time_to_go = (line_len - along_track_dist) / goal_->speed_control.value;
  feedback.running = true;

  result.success = true;

  // update the visualization
  visualization_msgs::Marker marker;
  marker.type = visualization_msgs::Marker::LINE_STRIP;
  marker.header.frame_id = adapter_->mapFrameId();
  marker.header.stamp = vehicleState.header.stamp;
  marker.pose.orientation.w = 1.0; // avoid uninitialized errors
  marker.ns = "stdgoal";
  marker.id = 0;
  marker.frame_locked = true;
  marker.points.resize(2);
  marker.points[0].x = en_start(0);
  marker.points[0].y = en_start(1);
  marker.points[0].z = -goal_->depth_start;
  marker.points[1].x = en_end(0);
  marker.points[1].y = en_end(1);
  marker.points[1].z = -goal_->depth_end;
  marker.scale.x = 0.25;
  marker.scale.y = marker.scale.x;
  marker.scale.z = 0.25;
  marker.color.r = 0;
  marker.color.g = 1;
  marker.color.b = 1;
  marker.color.a = 0.7;

  visual_ = visualization_msgs::MarkerArray();
  visual_.markers.push_back(marker);

  return std::make_tuple(done, feedback, result, ref);
}

} // namespace ds_stdgoal
