/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/13/20.
//

#include <ds_stdgoal/control_adapter.h>
#include <ds_libtrackline/Projection.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

namespace ds_stdgoal {

ControlAdapter::ControlAdapter() : nav_projection_(0,0) {
  // do nothing
}

void ControlAdapter::setupParameters(ros::NodeHandle& nh, ros::NodeHandle& nhp) {
  conn_ = ds_param::ParamConnection::create(nh);
  std::string act_con = ros::param::param<std::string>("~active_controller_dsparam", "");
  std::string act_alloc = ros::param::param<std::string>("~active_allocation_dsparam", "");
  std::string act_joy = ros::param::param<std::string>("~active_joystick_dsparam", "");

  if (act_con.empty() || act_alloc.empty() || act_joy.empty()) {
    ROS_ERROR_STREAM("Empty DSPARAM variable name!");
    ROS_ERROR_STREAM("    Active Controller: \"" <<act_con <<"\"");
    ROS_ERROR_STREAM("    Active Allocation: \"" <<act_alloc <<"\"");
    ROS_ERROR_STREAM("    Active Joystick: \"" <<act_joy <<"\"");
    ROS_BREAK();
  }
  active_controller_ = conn_->connect<ds_param::IntParam>(act_con);
  active_allocation_ = conn_->connect<ds_param::IntParam>(act_alloc);
  active_joystick_ = conn_->connect<ds_param::IntParam>(act_joy);

  // now we have to load some params
  surfaced_depth = ros::param::param<double>("~surfaced_depth", 0);
  double origin_lat = ros::param::param<double>("origin/latitude", 0);
  double origin_lon = ros::param::param<double>("origin/longitude", 0);

  ROS_INFO_STREAM("Loading origin: " <<origin_lat <<" x " <<origin_lon);
  nav_projection_ = ds_trackline::Projection(origin_lon, origin_lat);

  // load the frame id
  control_frame_id_ = ros::param::param<std::string>("~control_frame_id", "control_frame");
  ROS_INFO_STREAM("Using control frame ID \"" <<control_frame_id_ <<"\"");
  map_frame_id_ = ros::param::param<std::string>("~map_frame_id", "local_proj");
  ROS_INFO_STREAM("Using     map frame ID \"" <<map_frame_id_ <<"\"");

  // "you can't use ~ names with node handle methods", because ROS had to make a few terrible
  // design decisions along the way.  Don't we all. So instead we have this dual node handle nonsense.
  // load max rate values to use if we don't have anything set yet
  if (!nhp.getParam("maxrate_heading", hdg_maxrate)) {
    ROS_FATAL_STREAM("MUST set max heading rate limit");
  }
  if (!nhp.getParam("maxrate_depth", depth_maxrate)) {
    ROS_FATAL_STREAM("MUST set max depth rate limit");
  }
  ROS_INFO_STREAM("Heading maxrate: " <<hdg_maxrate <<" deg per sec");
  hdg_maxrate *= (M_PI/180.0);
  ROS_INFO_STREAM("Depth maxrate: " <<depth_maxrate <<" m/s");
}


void ControlAdapter::setupPublishers(ros::NodeHandle& nh) {
  extended_goal_publisher_ = nh.advertise<ds_control_msgs::RovControlGoal>("rov_goal", 10, false);
  goal_pose_pub = nh.advertise<geometry_msgs::PoseStamped>(ros::this_node::getName() + "goal_pose", 10);
  visualization_pub_ = nh.advertise<visualization_msgs::MarkerArray>(ros::this_node::getName() + "viz", 10, true);
}

void ControlAdapter::setupSubscriptions(ros::NodeHandle& nh) {
  ranges_sub_ = nh.subscribe("ranges", 10, &ControlAdapter::_ranges_callback, this);
}

void ControlAdapter::_ranges_callback(const ds_sensor_msgs::Ranges3D &msg) {
  last_range = msg;

  double accum=0;
  double denom=0;

  for (const auto& range : msg.ranges) {
    // for now, don't worry about rotating from inst. frame to
    if (range.range_validity == ds_sensor_msgs::Range3D::RANGE_VALID) {
      // not sure what this sign change is about.  We'll fix it later
      accum += -range.range.point.z;
      denom += 1.0;
    }
  }

  if (denom > 0) {
    current_altitude = accum / denom;
  }
}

void ControlAdapter::updateNavState(const ds_nav_msgs::NavState &state) {
  last_state = state;
}

const ds_nav_msgs::NavState& ControlAdapter::getLastNavState() const {
  return last_state;
}

void ControlAdapter::setControllerMode(int controller) {
  active_controller_->Set(controller);
}

void ControlAdapter::setAllocation(int allocation) {
  active_allocation_->Set(allocation);
}

void ControlAdapter::setJoystick(int joystick) {
  active_joystick_->Set(joystick);
}

bool ControlAdapter::isActiveJoystick(int my_joystick) const {
  return active_joystick_->Get() == my_joystick;
}

const std::string& ControlAdapter::controlFrameId() const {
  return control_frame_id_;
}

const std::string& ControlAdapter::mapFrameId() const {
  return map_frame_id_;
}

double ControlAdapter::pick_hdg_rate(double state, double state_rate, double goal,
      double goal_rate) const {

  state = state * M_PI/180.0;
  state_rate = state_rate * M_PI/180.0;

  //ROS_INFO_STREAM("HDG " <<state*180.0/M_PI <<" --> " <<goal*180.0/M_PI);
  if (std::isnan(goal_rate)) {
    double d_hdg = goal - state;
    // get a differential heading, goal-state, so that we can figure out which way to go
    if (d_hdg <= -M_PI) {
      d_hdg +=  2.0*M_PI;
    }
    if (d_hdg >= M_PI) {
      d_hdg -=  2.0*M_PI;
    }

    // if we're within +/- 5 degrees of exactly opposed, pick whichever direction
    // we're already rotating a bit
    if (fabs(d_hdg) > M_PI*(1.0 - 5.0/180.0)) {
      if (state_rate > 0) {
        //ROS_INFO_STREAM("d_hdg: " <<d_hdg*180.0/M_PI <<" -> +max because r=" <<state_rate);
        return hdg_maxrate;
      }
      //ROS_INFO_STREAM("d_hdg: " <<d_hdg*180.0/M_PI <<" -> -max because r=" <<state_rate);
      return -hdg_maxrate;
    }
    if (d_hdg >= 0) {
      //ROS_INFO_STREAM("d_hdg: " <<d_hdg*180.0/M_PI <<" -> +max");
      return hdg_maxrate;
    } else {
      //ROS_INFO_STREAM("d_hdg: " <<d_hdg*180.0/M_PI <<" -> -max");
      return -hdg_maxrate;
    }
  }

  ROS_INFO_STREAM("Just using goal_rate: " <<goal_rate);
  return goal_rate;
}

double ControlAdapter::pick_depth_rate(double state, double state_rate, double goal,
      double goal_rate) const {

  if (std::isnan(goal_rate)) {
    if (goal >= state) {
      //ROS_INFO_STREAM("depth: " <<state <<" --> " <<goal <<" implies +max");
      return depth_maxrate;
    } else {
      //ROS_INFO_STREAM("depth: " <<state <<" --> " <<goal <<" implies -max");
      return -depth_maxrate;
    }
  }
  //ROS_INFO_STREAM("depth: " <<state <<" --> " <<goal <<" has a rate set already!" <<goal_rate);

  return goal_rate;
}

void ControlAdapter::sendZero() {

  ds_control_msgs::RovControlGoal ret;
  ret.goal = invalidGoal();
  ret.goal.header.stamp = ros::Time::now();
  markValid(ret.goal.surge_u, 0);
  markValid(ret.goal.sway_v, 0);
  markValid(ret.goal.heave_w, 0);
  markValid(ret.goal.r, 0);

  extended_goal_publisher_.publish(ret);
}

const ds_trackline::Projection& ControlAdapter::controllerProjection() const {
  return nav_projection_;
}

void ControlAdapter::initSpeedRef(const ds_nav_msgs::NavState& state, const ds_mx_stdprimitives::Speed& speed) {
  switch (speed.mode) {
    case ds_mx_stdprimitives::Speed::SPEEDMODE_OPENLOOP:
      break;
    case ds_mx_stdprimitives::Speed::SPEEDMODE_CLOSEDLOOP:
      break;
    default:
      ROS_ERROR_STREAM("UNKNOWN SPEED MODE!, num=" <<speed.mode);
  }
}

void ControlAdapter::updateSpeedRef(ds_control_msgs::RovControlGoal& ref, const ds_nav_msgs::NavState& state, const ds_mx_stdprimitives::Speed& speed) {
  // actually doesn't matter what the speed mode is, oddly enough
  markValid(ref.goal.surge_u, speed.value);
  markValid(ref.goal.sway_v, 0.15);

  switch (speed.mode) {
    case ds_mx_stdprimitives::Speed::SPEEDMODE_OPENLOOP:
      ref.auto_xy = false;
      break;
    case ds_mx_stdprimitives::Speed::SPEEDMODE_CLOSEDLOOP:
      ref.auto_xy = true;
      break;
    default:
      ROS_ERROR_STREAM("UNKNOWN SPEED MODE!, num=" <<speed.mode);
  }
}

void ControlAdapter::initDepthRef(const ds_nav_msgs::NavState& state, const ds_mx_stdprimitives::Depth& depth) {
  switch (depth.depth_mode) {
    case ds_mx_stdprimitives::Depth::DEPTHMODE_SURFACE:
      break;
    case ds_mx_stdprimitives::Depth::DEPTHMODE_TRIANGLE:
      // FALLTHROUGH
    case ds_mx_stdprimitives::Depth::DEPTHMODE_ALTITUDE:
      // initially control to our current state
      current_depth_goal = state.down;
      current_depth_time = state.header.stamp;
      current_depth_sign = 1.0;
      // FALLTHROUGH
    case ds_mx_stdprimitives::Depth::DEPTHMODE_DEPTH:
      // don't set a goal for depth-- it'll happen on the updateDepthRef next time
      break;
    default:
      ROS_ERROR_STREAM("UNKNOWN DEPTH MODE!, num=" <<depth.depth_mode);
  }
}

void ControlAdapter::updateDepthRef(ds_control_msgs::RovControlGoal& ref, const ds_nav_msgs::NavState& state, const ds_mx_stdprimitives::Depth& depth) {
  switch (depth.depth_mode) {
    case ds_mx_stdprimitives::Depth::DEPTHMODE_SURFACE:
      if (state.down > surfaced_depth) {
        markValid(ref.goal.down, -1.0); // drive up at full speed
        ref.auto_depth = false;
      }
      break;
    case ds_mx_stdprimitives::Depth::DEPTHMODE_DEPTH:
      markValid(ref.goal.down, depth.depth);
      markValid(ref.goal.heave_w, pick_depth_rate(state.down, state.heave_w, depth.depth));
      ref.auto_depth = true;
      break;
    case ds_mx_stdprimitives::Depth::DEPTHMODE_ALTITUDE: {
      // compute the target depth, and subject it to ceiling / floor
      double target_depth = state.down + current_altitude - depth.altitude;
      //ROS_INFO_STREAM("DEPTH: Now: " <<state.down <<" CA: " <<current_altitude
      //              <<" GA: " <<depth.altitude <<" TGT: " <<target_depth
      //              <<" FL: " <<depth.depth_floor <<" CL: " <<depth.depth_ceiling);
      if (!std::isnan(depth.depth_ceiling)) {
        target_depth = std::max<double>(target_depth, depth.depth_ceiling);
      }
      if (!std::isnan(depth.depth_floor)) {
        target_depth = std::min<double>(target_depth, depth.depth_floor);
      }
      markValid(ref.goal.down, target_depth);
      markValid(ref.goal.heave_w, pick_depth_rate(state.down, state.heave_w, depth.depth));
      ref.auto_depth = true;
      break;
    }
    case ds_mx_stdprimitives::Depth::DEPTHMODE_TRIANGLE: {

      // top depth is tricky to find
      double ceiling = surfaced_depth;
      if (!std::isnan(depth.depth_ceiling)) {
        ceiling = std::max<double>(depth.depth_ceiling, ceiling);
      }

      // update depth goal based on current direction
      double dt = (state.header.stamp - current_depth_time).toSec();
      current_depth_goal += dt * current_depth_sign * depth.triangle_rate;

      // check for turn-around
      // give the depth floor priority
      if (current_depth_goal >= depth.depth_floor) {
        current_depth_goal = depth.depth_floor;
        current_depth_sign = -1.0; // go up
      } else if (!std::isnan(depth.min_altitude) && current_altitude <= depth.min_altitude) {
        // checks for minimum altitude
        // set to deepest depth that satisfies altitude goal
        current_depth_goal = state.down + current_altitude - depth.min_altitude;
        current_depth_sign = -1.0; // go up
      } else if (current_depth_goal <= ceiling) {
        current_depth_goal = ceiling;
        current_depth_sign = 1.0; // go down
      }
      markValid(ref.goal.down, current_depth_goal);
      markValid(ref.goal.heave_w, pick_depth_rate(state.down, state.heave_w, depth.depth,
                                                  current_depth_sign * depth.triangle_rate));
      ref.auto_depth = true;
      ROS_ERROR_STREAM("TRIANGLE depth mode not yet supported!");
      break;
    }
    default:
      ROS_ERROR_STREAM("UNKNOWN DEPTH MODE! num=" <<depth.depth_mode);
  }
}

ds_nav_msgs::AggregatedState ControlAdapter::invalidGoal() {
  ds_nav_msgs::AggregatedState ret;

  markInvalid(ret.northing);
  markInvalid(ret.easting);
  markInvalid(ret.down);
  markInvalid(ret.roll);
  markInvalid(ret.pitch);
  markInvalid(ret.heading);
  markInvalid(ret.surge_u);
  markInvalid(ret.sway_v);
  markInvalid(ret.heave_w);
  markInvalid(ret.p);
  markInvalid(ret.q);
  markInvalid(ret.r);
  markInvalid(ret.du_dt);
  markInvalid(ret.dv_dt);
  markInvalid(ret.dw_dt);
  markInvalid(ret.dp_dt);
  markInvalid(ret.dq_dt);
  markInvalid(ret.dr_dt);

  return ret;
}

void ControlAdapter::markInvalid(ds_nav_msgs::FlaggedDouble& val) {
  val.valid = false;
  val.value = 0;
}

void ControlAdapter::markValid(ds_nav_msgs::FlaggedDouble& val, double setpoint) {
  if (std::isnan(setpoint)) {
    markInvalid(val);
  } else {
    val.valid = true;
    val.value = setpoint;
  }
}

void ControlAdapter::publishExtendedGoal(const ds_control_msgs::RovControlGoal& ref)  {
  extended_goal_publisher_.publish(ref);

  // also publishes the pose
  geometry_msgs::PoseStamped ps;

  ps.header = ref.goal.header;
  tf2::Quaternion q;
  if (ref.goal.heading.valid) {
    q.setRPY(0, 0, M_PI/2-ref.goal.heading.value);
  } else {
    q.setRPY(0, 0, M_PI/2-last_state.heading*M_PI/180.0);
  }
  ps.pose.orientation = tf2::toMsg(q);

  if (ref.goal.easting.valid) {
    ps.pose.position.x = ref.goal.easting.value;
  } else {
    ps.pose.position.x = last_state.easting;
  }

  if (ref.goal.northing.valid) {
    ps.pose.position.y = ref.goal.northing.value;
  } else {
    ps.pose.position.y = last_state.northing;
  }

  if (ref.goal.down.valid) {
    ps.pose.position.z = -ref.goal.down.value;
  } else {
    ps.pose.position.z = -last_state.down;
  }

  goal_pose_pub.publish(ps);
}

void ControlAdapter::publishVisual(const visualization_msgs::MarkerArray &visual) {
  if (visualization_pub_) {
    visualization_pub_.publish(visual);
  }
}

void ControlAdapter::clearVisual() {
  visualization_msgs::MarkerArray visual;

  ROS_INFO_STREAM("Clearing markers...");
  visualization_msgs::Marker clear_marker;
  clear_marker.action = visualization_msgs::Marker::DELETEALL;
  visual.markers.push_back(clear_marker);

  publishVisual(visual);
}

} // namespace ds_stdgoal
