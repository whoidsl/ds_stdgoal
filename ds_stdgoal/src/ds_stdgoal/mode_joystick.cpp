/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/9/20.
//

#include <ds_stdgoal/mode_joystick.h>

namespace ds_stdgoal {

ModeJoystick::ModeJoystick(const std::shared_ptr<ControlAdapter>& _adapter)
: ParentT(_adapter, "action_joystick") {
  // do nothing
}

void ModeJoystick::fillInRef(ds_control_msgs::RovControlGoal& ref, const ds_nav_msgs::NavState& vehicleState) {
  // note: mark valid will automatically NOT mark NaN values as valid
  adapter_->markValid(ref.goal.surge_u, goal_->fwd);
  adapter_->markValid(ref.goal.sway_v, goal_->stbd);
  adapter_->markValid(ref.goal.heave_w, goal_->down);
  adapter_->markValid(ref.goal.r, goal_->heading);
  adapter_->markValid(ref.goal.q, goal_->pitch);
  adapter_->markValid(ref.goal.p, goal_->roll);

  ref.auto_heading = goal_->auto_heading;
  ref.auto_depth = goal_->auto_depth;
  ref.auto_xy = goal_->auto_xy;

  visualization_msgs::Marker marker;
  marker.type = visualization_msgs::Marker::ARROW;
  marker.header.frame_id = adapter_->controlFrameId();
  marker.header.stamp = vehicleState.header.stamp;
  marker.ns = "stdgoal";
  marker.id = 0;
  marker.frame_locked = true;
  marker.points.resize(2);
  marker.points[0].x = 0;
  marker.points[0].y = 0;
  marker.points[0].z = 0;
  marker.points[1].x = goal_->fwd;
  marker.points[1].y = -goal_->stbd;
  marker.points[1].z = -goal_->down;
  double len = sqrt(goal_->fwd * goal_->fwd + goal_->stbd * goal_->stbd +
      goal_->down * goal_->down);
  marker.scale.x = len*0.25;
  marker.scale.y = len*0.35;
  marker.scale.z = len*0.25;
  marker.color.r = 0;
  marker.color.g = 1;
  marker.color.b = 1;
  marker.color.a = 1;

  visual_ = visualization_msgs::MarkerArray();
  visual_.markers.push_back(marker);
}

} // namespace ds_stdgoal