/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/8/20.
//

#include <ds_stdgoal/stdgoal.h>
#include <ds_util/ds_util.h>
#include "stdgoalPrivate.h"
#include <ds_control_msgs/JoystickEnum.h>

#include <ds_stdgoal/mode_idle.h>
#include <ds_stdgoal/mode_joystick.h>
#include <ds_stdgoal/mode_loiter.h>
#include <ds_stdgoal/mode_loiter_surface.h>
#include <ds_stdgoal/mode_manual.h>
#include <ds_stdgoal/mode_trackline.h>
#include <ds_stdgoal/mode_trackline3d.h>
#include <geometry_msgs/PoseStamped.h>

namespace ds_stdgoal {

StdGoal::StdGoal() : ds_control::GoalBase(),
d_ptr_(new StdGoalPrivate)
{
}

StdGoal::StdGoal(int argc, char** argv, const std::string& name):
  ds_control::GoalBase::GoalBase(argc, argv, name),
  d_ptr_(new StdGoalPrivate) {
}

StdGoal::~StdGoal() = default;

uint64_t StdGoal::type() const noexcept {
  return ds_control_msgs::JoystickEnum::STDGOAL;
}

void StdGoal::vehicleStateCallback(const ds_nav_msgs::NavState& vehicleState) {
  DS_D(StdGoal);

  if (d->adapter->isActiveJoystick()) {
    for (auto& action : d->actions) {
      if (action->isRunning()) {
        action->vehicle_state_cb(vehicleState);
        return;
      }
    }
  } else {
    for (auto& action : d->actions) {
      action->cancelAll();
    }
  }
}

void StdGoal::setup() {
  DS_D(StdGoal);

  // create our I/O adapter
  d->adapter.reset(new ControlAdapter);

  // create our action servers
  d->actions.push_back(std::shared_ptr<ActionMode>(new ModeIdle(d->adapter)));
  d->actions.push_back(std::shared_ptr<ActionMode>(new ModeJoystick(d->adapter)));
  d->actions.push_back(std::shared_ptr<ActionMode>(new ModeLoiter(d->adapter)));
  d->actions.push_back(std::shared_ptr<ActionMode>(new ModeLoiterSurface(d->adapter)));
  d->actions.push_back(std::shared_ptr<ActionMode>(new ModeManual(d->adapter)));
  d->actions.push_back(std::shared_ptr<ActionMode>(new ModeTrackline(d->adapter)));
  d->actions.push_back(std::shared_ptr<ActionMode>(new ModeTrackline3d(d->adapter)));

  // initialize everything as normal
  ds_control::GoalBase::setup();

  // setup action servers
  auto nh = nodeHandle();
  for (auto & action : d->actions) {
    action->setupActionServer(nh);
  }
}

void StdGoal::setupParameters() {
  ds_control::GoalBase::setupParameters();
  DS_D(StdGoal);

  auto nh = nodeHandle();
  auto nhp = nodeHandle("~");
  d->adapter->setupParameters(nh, nhp);

  for (auto & action : d->actions) {
    action->setupParameters(nh);
  }

 // TODO
}

void StdGoal::setupSubscriptions() {
  ds_control::GoalBase::setupSubscriptions();
  DS_D(StdGoal);

  auto nh = nodeHandle();
  d->adapter->setupSubscriptions(nh);
  for (auto & action : d->actions) {
    action->setupSubscriptions(nh);
  }

  d->navstate_update_sub = nh.subscribe("navstate", 10, &StdGoal::vehicleStateCallback, this);
}

void StdGoal::setupPublishers() {
  ds_control::GoalBase::setupPublishers();
  DS_D(StdGoal);

  auto nh = nodeHandle();
  for (auto & action : d->actions) {
    action->setupPublishers(nh);
  }

  d->adapter->setupPublishers(nh);
}



} // namespace ds_stdgoal

