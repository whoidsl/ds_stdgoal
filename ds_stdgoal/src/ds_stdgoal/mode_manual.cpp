/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/9/20.
//

#include <ds_stdgoal/mode_manual.h>

namespace ds_stdgoal {

ModeManual::ModeManual(const std::shared_ptr<ControlAdapter>& _adapter)
: ParentT(_adapter, "action_manual") {
  // do nothing
}

void ModeManual::onStart(const typename ParentT::GoalConstPtr &goal) {
  ParentT::onStart(goal);

  // custom stuff
  heading_setpoint = goal_->heading;
  adapter_->initSpeedRef(adapter_->getLastNavState(), goal_->speed_control);
  adapter_->initDepthRef(adapter_->getLastNavState(), goal_->depth_control);
}

void ModeManual::fillInRef(ds_control_msgs::RovControlGoal& ref, const ds_nav_msgs::NavState& vehicleState) {
  double dt = (vehicleState.header.stamp - last_update).toSec();
  double hdg_rate_rps = std::numeric_limits<double>::quiet_NaN();
  if (!std::isnan(goal_->heading_rate)) {
    hdg_rate_rps = goal_->heading_rate * M_PI/180.0;
  }
  heading_setpoint += dt * hdg_rate_rps;

  adapter_->markValid(ref.goal.heading, heading_setpoint);
  adapter_->markValid(ref.goal.r, adapter_->pick_hdg_rate(
        vehicleState.heading, vehicleState.r, heading_setpoint, hdg_rate_rps));
  adapter_->updateSpeedRef(ref, vehicleState, goal_->speed_control);
  adapter_->updateDepthRef(ref, vehicleState, goal_->depth_control);
  ref.auto_heading = true;

  // not really sure what to do about visualization here
}

} // namespace ds_stdgoal