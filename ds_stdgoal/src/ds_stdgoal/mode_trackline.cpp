/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 7/9/20.
//

#include <ds_stdgoal/mode_trackline.h>
#include <ds_util/ds_util.h>

namespace ds_stdgoal {

ModeTrackline::ModeTrackline(const std::shared_ptr<ControlAdapter>& _adapter)
    : ParentT(_adapter, "action_trackline"), proj_(0, 0), line_(0, 0, 0, 1) {
  kappa_ = 0.3;
  sway_ = 0.1;
}

void ModeTrackline::setupParameters(ros::NodeHandle &nh) {
  ParentT::setupParameters(nh);

  kappa_ = ros::param::param("~trackline_kappa", kappa_);
  sway_ = ros::param::param("~trackline_sway", sway_);
}

void ModeTrackline::onStart(const GoalConstPtr &goal) {
  ROS_INFO_STREAM("Starting Trackline stdprim!");

  // setup control modes
  adapter_->setJoystick(ds_control_msgs::JoystickEnum::STDGOAL);
  adapter_->setControllerMode(ds_control_msgs::ControllerEnum::ROV);
  adapter_->setAllocation(ds_control_msgs::RovAllocationEnum::ROV);

  // save goal
  goal_ = goal;

  // grab our projection
  proj_ = adapter_->controllerProjection();

  // fill in our trackline
  line_ = ds_trackline::Trackline(goal_->longitude_start, goal_->latitude_start,
      goal_->longitude_end, goal_->latitude_end);
}

void ModeTrackline::onStop() {
  adapter_->sendZero();
}

std::tuple<bool, ModeTrackline::Feedback, ModeTrackline::Result, ds_control_msgs::RovControlGoal>
    ModeTrackline::vehicleStateCallback(const ds_nav_msgs::NavState &vehicleState) {
  bool done = false;
  typename ParentT::Feedback feedback;
  typename ParentT::Result result;

  // prepare our return value
  ds_control_msgs::RovControlGoal ref;
  ref.goal = adapter_->invalidGoal();
  ref.goal.header = vehicleState.header;
  ref.goal.ds_header = vehicleState.ds_header;

  // next, we need to compute a heading goal.
  // start by making sure the line is valid
  if (line_.getLength() < 0.001) {
    ROS_ERROR_STREAM("Trackline too short!  Heading is ill-defined, setting to zero");

    ref.goal.heading.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
    ref.goal.heading.value = 0;
    ref.goal.r.valid = true; // don't turn
    ref.goal.r.value = 0;

    feedback.distance_to_go = 0;
    feedback.time_to_go = 0;
    feedback.running = false;

    result.success = false;

    return std::make_tuple(true, feedback, result, ref);
  }

  // make sure position is valid before override the initial guess of just using the
  // nominal course
  double heading_setpoint = line_.getCourseRad();
  // ALWAYS autoheading!
  ref.auto_heading = true;
  if (!std::isnan(vehicleState.lon) && !std::isnan(vehicleState.lat)) {
    auto alongacross = line_.lonlat_to_trackframe(vehicleState.lon, vehicleState.lat);
    double off_line_vect = -alongacross(1); // Dana's old sign convention is backwards from the new trackline library.
    double sign_of_vect = ds_util::sgn(off_line_vect);

    // modify the setpoint by an amount to bring us towards the line
    double heading_change = fabs(off_line_vect) * kappa_;
    heading_setpoint += sign_of_vect * std::min<double>(M_PI / 3.0, heading_change);

    // check if we've passed the end of the line
    ds_trackline::Trackline::VectorEN goal_line(alongacross(0), 0);
    if (alongacross(0) > 0) {
      // we've passed the end of the line!  SUCCESS!
      done = true;
      result.success = true;
      // ... but send a command to hold position AT the endpoint
      goal_line = ds_trackline::Trackline::VectorEN::Zero();
    } else {
      // of the DESIRED vehicle path from current location along heading_setpoint and the trackline.
      // Note that there's a divide-by-zero issue when very close to the trackline-- but convienently,
      // the limit of the desired function does exist.  So we just use that when close.  Very smooth.
      // Aside from the discontinuity, differentiable even!

      // start at vehicle position projected onto line
      goal_line = ds_trackline::Trackline::VectorEN(alongacross(0), 0);
      if (heading_change > 0.01) {
        goal_line(0) += fabs(off_line_vect)/tan(heading_change);
      } else {
        // limit of the above function as distance off line --> 0
        // This is one of those 3 times you get to use l'Hopital's rule!
        // we neglect the abs (as everything's positive) and the limit to 30 degrees
        // (as that's what the other if case is for), and solve the well-behaved limit as:
        // lim_{x->0} x / tan(kappa * x)
        //          = lim_{x->0} d/dx(x) / d/dx(tan(kappa * x)) // by l'hopital
        //          = lim_{x->0} 1 / sec^2(kappa*x)*kappa       // by differentiating things
        //          = 1 / 1.0 * kappa                           // by sec(0) = 0
        //          = 1 / kappa                // <--- Hey, that's easy to use!

        goal_line(0)+= 1 / kappa_;
      }
    }
    // goal_line is now in the goal frame-- but we need it in the global frame, then converted
    // to controller x/y
    auto goal_ll = line_.trackframe_to_lonlat(goal_line);
    ds_trackline::Projection::VectorEN goal_en = proj_.lonlat2projected(goal_ll);

    // do this here
    adapter_->markValid(ref.goal.easting, goal_en(0));
    adapter_->markValid(ref.goal.northing, goal_en(1));

    // now update our distance-to-go
    feedback.running = !done;
    // yes, we'll let these go negative.  Should be done anyway by then
    feedback.distance_to_go = -alongacross(0); // sign convention is backwards from
    // what users will expect
    // There's a whole bunch of speed modes and stuff, so just use
    // measured forward speed.
    feedback.time_to_go = feedback.distance_to_go / vehicleState.surge_u;
  } else {
    adapter_->markInvalid(ref.goal.easting);
    adapter_->markInvalid(ref.goal.northing);
  }

  // update reference for everything
  adapter_->markValid(ref.goal.heading, heading_setpoint);
  adapter_->markValid(ref.goal.r, adapter_->pick_hdg_rate(
      vehicleState.heading, vehicleState.r, heading_setpoint));
  adapter_->updateSpeedRef(ref, vehicleState, goal_->speed_control);
  adapter_->updateDepthRef(ref, vehicleState, goal_->depth_control);

  // update the visualization
  auto viz_start_en = proj_.lonlat2projected(line_.getStartLL());
  auto viz_end_en = proj_.lonlat2projected(line_.getEndLL());

  visualization_msgs::Marker marker;
  marker.type = visualization_msgs::Marker::LINE_STRIP;
  marker.header.frame_id = adapter_->mapFrameId();
  marker.header.stamp = vehicleState.header.stamp;
  marker.pose.orientation.w = 1.0; // avoid uninitialized errors
  marker.ns = "stdgoal";
  marker.id = 0;
  marker.frame_locked = true;
  marker.points.resize(2);
  marker.points[0].x = viz_start_en(0);
  marker.points[0].y = viz_start_en(1);
  marker.points[1].x = viz_end_en(0);
  marker.points[1].y = viz_end_en(1);
  marker.scale.x = 0.25;
  marker.scale.y = marker.scale.x;
  marker.scale.z = 0.25;
  // update based on depth mode
  switch (goal_->depth_control.depth_mode) {
    case ds_mx_stdprimitives::Depth::DEPTHMODE_DEPTH:
      marker.points[0].z = -goal_->depth_control.depth;
      break;
    case ds_mx_stdprimitives::Depth::DEPTHMODE_ALTITUDE:
      marker.points[0].z = -ref.goal.down.value;
      break;
    case ds_mx_stdprimitives::Depth::DEPTHMODE_SURFACE:
      marker.points[0].z = 0;
      break;

    case ds_mx_stdprimitives::Depth::DEPTHMODE_TRIANGLE:
      double half_range = 0.5*(goal_->depth_control.depth_floor - goal_->depth_control.depth_ceiling);
      double midpt = 0.5*(goal_->depth_control.depth_floor + goal_->depth_control.depth_ceiling);
      marker.points[0].z = -midpt;
      marker.scale.z = half_range;
      break;
  }
  marker.points[1].z = marker.points[0].z;
  marker.color.r = 0;
  marker.color.g = 1;
  marker.color.b = 1;
  marker.color.a = 0.7;

  visual_ = visualization_msgs::MarkerArray();
  visual_.markers.push_back(marker);

  return std::make_tuple(done, feedback, result, ref);
}

} // namespace ds_stdgoal
